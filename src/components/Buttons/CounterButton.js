import React from 'react';

class CounterButton extends React.Component {
	render() {
		return (
			<div>
				<button onClick={this.props.click}>{this.props.name}</button>
			</div>
		);
	}
}

export default CounterButton;
