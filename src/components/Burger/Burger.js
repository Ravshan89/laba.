import React from 'react';
import Ingredient from '../Ingredient/Ingredient';

const Burger = (props) => {
	return (
		<div className="products">
			{props.ingredients.map((ingr) => {
				return (
					<Ingredient
						key={ingr.id}
						count={ingr.count}
						name={ingr.name}
						increase={() => props.increase(ingr.id)}
						decrease={() => props.decrease(ingr.id)}
					/>
				);
			})}
		</div>
	);
};

export default Burger;
