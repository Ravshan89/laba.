import React from 'react';
import CounterButton from '../Buttons/CounterButton';
import './ingredient.css';

function Ingredient(props) {
	return (
		<div className="buttons">
			<p>{props.name}</p>
			<div className="button">
				<CounterButton name="Less" value={props.count} click={props.decrease} />
				<CounterButton name="More" click={props.increase} />
			</div>
		</div>
	);
}

export default Ingredient;
