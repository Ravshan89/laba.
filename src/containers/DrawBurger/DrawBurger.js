import React from 'react';
import '../BurgerConstructor.css';

const DrawBurger = (props) => {
	let salad = [];
	for (let i = 0; i < props.saladCount; i++) {
		salad.push(<div class="Salad" />);
	}

	let bacon = [];
	for (let i = 0; i < props.baconCount; i++) {
		bacon.push(<div class="Bacon" />);
	}

	let cheese = [];
	for (let i = 0; i < props.cheeseCount; i++) {
		cheese.push(<div class="Cheese" />);
	}

	let meat = [];
	for (let i = 0; i < props.meatCount; i++) {
		meat.push(<div class="Meat" />);
	}

	return (
		<div className="Burger">
			<div className="BreadTop">
				<div className="Seeds1" />
				<div className="Seeds2" />
			</div>

			{salad}
			{bacon}
			{cheese}
			{meat}

			<div className="BreadBottom" />
		</div>
	);
};

export default DrawBurger;
