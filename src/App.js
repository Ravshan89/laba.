import React from 'react';
import './App.css';
import BurgerConstructor from './containers/BurgerConstructor';
import Constructor from './containers/DrawBurger/DrawBurger';

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<BurgerConstructor />
			</header>
		</div>
	);
}
export default App;
